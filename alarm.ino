#define DIODE_PIN 8
#define BUZZER_PIN 11

#define SENSOR_PIN 2

boolean isEnabled = false;
volatile int changeCount = 0;

void setup() {
  pinMode(SENSOR_PIN, INPUT);
  pinMode(DIODE_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  
  Serial.begin (9600);

  attachInterrupt(digitalPinToInterrupt(SENSOR_PIN), handleChange, CHANGE);
}

void handleChange() {
  int sensorState = digitalRead(SENSOR_PIN);
  if (sensorState == HIGH) {
    digitalWrite(BUZZER_PIN, LOW);
  } else {
    changeCount++;
    digitalWrite(BUZZER_PIN, HIGH);
  }
}

void loop() {
  while (!isEnabled) {
    if (digitalRead(SENSOR_PIN)  == HIGH) {
      digitalWrite(DIODE_PIN, HIGH);
      isEnabled = true;
    }
  }
  Serial.println(changeCount);
}
